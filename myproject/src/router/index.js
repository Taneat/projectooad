import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/company',
    name: 'Company',
    component: () => import('../views/Compa/Company.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  },
  {
    path: '/jobseeker',
    name: 'Jobseeker',
    component: () => import('../views/JobS/Jobseeker.vue')
  },
  {
    path: '/jobposting',
    name: 'Jobposting',
    component: () => import('../views/Jobposting/Jobposting.vue')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search/Search.vue')
  },
  {
    path: '/showdetail',
    name: 'Showdetail',
    component: () => import('../views/ShowDetail/Showdetail.vue')
  },
  {
    path: '/companycheck',
    name: 'CompanyCheck',
    component: () => import('../views/CompaCheck/CompaCheck.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

const Jobposting = require('../models/Jobposting')
const jobpostingController = {
  async getUsers (req, res, next) {
    // res.json(jobseekerController.getUsers())
    try {
      const users = await Jobposting.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Jobposting.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = jobpostingController

const Login = require('../models/Login')
const LoginController = {
  userList: [
    { id: 1, username: 'Nutthawut', password: '12345678' },
    { id: 2, username: 'Siriyakorn', password: '87654321' }
  ],
  listId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(LoginController.addUser(payload))
    const user = new Login(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(LoginController.updateUser(payload))
    try {
      const user = await Login.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(LoginController.deleteUser(id))
    try {
      const user = await Login.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    // res.json(LoginController.getUsers())
    try {
      const users = await Login.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    // res.json(LoginController.getUser(id))
    try {
      const user = await Login.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = LoginController

const Jobseeker = require('../models/Jobseeker')
const jobseekerController = {
  userList: [
    { id: 1, fname: 'Taneat', lname: 'Taddsab', idcard: '1258866477859', gender: 'M', age: 20, address: 'chonburi', tel: '0999999999' },
    { id: 2, fname: 'Saharud', lname: 'Seerakoon', idcard: '1278899654258', gender: 'M', age: 20, address: 'chonburi', tel: '0911111111' },
    { id: 3, fname: 'Siriphonpha', lname: 'Fangnakham', idcard: '1276633211458', gender: 'F', age: 20, address: 'chonburi', tel: '0987654321' }
  ],
  listId: 4,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(jobseekerController.addUser(payload))
    const user = new Jobseeker(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(jobseekerController.updateUser(payload))
    try {
      const user = await Jobseeker.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(jobseekerController.deleteUser(id))
    try {
      const user = await Jobseeker.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    // res.json(jobseekerController.getUsers())
    try {
      const users = await Jobseeker.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    // res.json(usersController.getUser(id))
    // User.findById(id).then(function (user) {
    //   res.json(user)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    try {
      const user = await Jobseeker.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = jobseekerController

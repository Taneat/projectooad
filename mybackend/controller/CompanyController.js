const Company = require('../models/Company')
const companyController = {
  userList: [
    { id: 1, companyname: 'Patcharawalai', address: '1/11 ต.บางที อ.บางครั้ง จ.บางวัน', email: 'AAA@gmail.com', tel: '0955555555' },
    { id: 2, companyname: 'Sasiwimol', address: '2/22 ต.ตรงนั้น อ.ตรงนี้ จ.ตรงนู้น', email: 'BBB@gmail.com', tel: '0944444444' }
  ],
  listId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(companyController.addUser(payload))
    const user = new Company(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(companyController.updateUser(payload))
    try {
      const user = await Company.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(companyController.deleteUser(id))
    try {
      const user = await Company.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    // res.json(companyController.getUsers())
    try {
      const users = await Company.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    // res.json(companyController.getUser(id))
    try {
      const user = await Company.findById(id)
      console.log(user)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = companyController

const mongoose = require('mongoose')
const jobseekerSchema = new mongoose.Schema({
  fname: String,
  lname: String,
  idcard: String,
  gender: String,
  age: { type: Number, min: 18, max: 65 },
  address: String,
  tel: String
})

module.exports = mongoose.model('Jobseeker', jobseekerSchema)

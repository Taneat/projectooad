const express = require('express')
const router = express.Router()
// const usersController = require('../controller/usersController')
const User = require('../models/User')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/mydb',
  { useNewUrlParser: true, useUnifiedTopology: true })

// const db = mongoose.connection
// db.on('error', console.error.bind(console, 'connection error:'))
// db.once('open', function () {
//   console.log('connect')
// })
/* GET users listing. */
router.get('/', async (req, res, next) => {
  // res.json(usersController.getUsers())
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.getUser(id))
  // User.findById(id).then(function (user) {
  //   res.json(user)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
  try {
    const user = await User.findById(id)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.post('/', (req, res, next) => {
  const payload = req.body
  // res.json(usersController.addUser(payload))
  const user = new User(payload)
  try {
    user.save()
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.put('/', async (req, res, next) => {
  const payload = req.body
  // res.json(usersController.updateUser(payload))
  try {
    const user = await User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})
router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(usersController.deleteUser(id))
  try {
    const user = await User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

module.exports = router

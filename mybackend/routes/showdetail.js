const express = require('express')
const router = express.Router()
const JobpostingController = require('../controller/JobpostingController')

router.get('/', JobpostingController.getUsers)
router.get('/:id', JobpostingController.getUser)

module.exports = router

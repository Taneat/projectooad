const express = require('express')
const router = express.Router()
const companyController = require('../controller/CompanyController')

/* GET users listing. */
router.get('/', companyController.getUsers)
router.get('/:id', companyController.getUser)
router.post('/', companyController.addUser)
router.put('/', companyController.updateUser)
router.delete('/:id', companyController.deleteUser)

module.exports = router
